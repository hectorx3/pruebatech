package example.micronaut.application

interface UseCase<in T, out O> {
    fun handle(command: T): O
}