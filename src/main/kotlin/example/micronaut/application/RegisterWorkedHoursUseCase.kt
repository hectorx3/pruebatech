package example.micronaut.application

import example.micronaut.EmpleadoRepository
import example.micronaut.WorkerRepository
import example.micronaut.domain.EmployessWorked
import example.micronaut.infrastructure.EmployeeDTO
import jakarta.inject.Inject
import jakarta.inject.Singleton
import java.time.LocalDate

@Singleton
class RegisterWorkedHoursUseCase
@Inject constructor(
    private val workerRepository: WorkerRepository
) : UseCase<EmployessWorked, EmployeeDTO>{
    override fun handle(command: EmployessWorked): EmployeeDTO {
        var isAfterDate = LocalDate.now().isAfter(command.worked_date)
        if(command.worked_hours <= 0 || command.worked_hours > 20  || command.employee_id == null || !  isAfterDate) return EmployeeDTO(-1, false)

        var findEmp = workerRepository.findByEmployee_idAndWorked_date(command.employee_id, command.worked_date).firstOrNull()

        if(findEmp == null) {
            var nuevoEmp = workerRepository.save(command.employee_id, command.worked_hours, command.worked_date)
            return EmployeeDTO(nuevoEmp.employee_id.toInt(), true)
        }

        val totalHours = findEmp.worked_hours + command.worked_hours
        if(totalHours <= 20) {var isAfterDate = LocalDate.now().isAfter(command.worked_date)
            if(command.worked_hours <= 0 || command.worked_hours > 20  || command.employee_id == null || !  isAfterDate) return EmployeeDTO(-1, false)

            var findEmp = workerRepository.findByEmployee_idAndWorked_date(command.employee_id, command.worked_date).firstOrNull()

            if(findEmp == null) {
                var nuevoEmp = workerRepository.save(command.employee_id, command.worked_hours, command.worked_date)
                return EmployeeDTO(nuevoEmp.employee_id.toInt(), true)
            }

            val totalHours = findEmp.worked_hours + command.worked_hours
            if(totalHours <= 20) {
                val updateEmp = workerRepository.update(command.employee_id, totalHours)
                return EmployeeDTO(command.employee_id.toInt(), true)
            }

            return EmployeeDTO(-1, false)
            val updateEmp = workerRepository.update(command.employee_id, totalHours)
            return EmployeeDTO(command.employee_id.toInt(), true)
        }

        return EmployeeDTO(-1, false)
    }

}