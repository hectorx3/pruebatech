package example.micronaut.application

import example.micronaut.*
import example.micronaut.domain.*
import example.micronaut.infrastructure.PaymentDTO
import example.micronaut.infrastructure.TotalHoursDTO
import io.micronaut.data.model.Pageable
import jakarta.inject.Inject
import jakarta.inject.Singleton
import java.math.BigDecimal
import java.time.LocalDate
import javax.validation.Valid

@Singleton
open class QueryCompany
@Inject constructor(
    private val genderRepository: GenderRepository,
    private val jobRepository: JobRepository,
    private val empRepository: EmpleadoRepository,
    private val workerRepository: WorkerRepository,
    private val fullInformationRepository: FullInformationRepository
) {
    open fun findAllGenders(@Valid pageable: Pageable): List<Genders> {
        return genderRepository.findAll().toList()
    }

    open fun findAllJobs(@Valid pageable: Pageable): List<Jobs> {
        return jobRepository.findAll().toList()
    }

    open fun getTotalHoursWorked(id: Long?,
                                 startDate: LocalDate?,
                                 endDate: LocalDate?
    ): TotalHoursDTO {
        if(id == null || startDate == null || endDate == null) return TotalHoursDTO(-1, false)
        val total = workerRepository.findHoras(id, startDate, endDate).sumOf { it.worked_hours }
        return TotalHoursDTO(total, total > 0)
    }

    open fun getPaymentHoursWorked(id: Long?,
                                 startDate: LocalDate?,
                                 endDate: LocalDate?
    ): PaymentDTO {
        if(id == null || startDate == null || endDate == null) return PaymentDTO(BigDecimal.ZERO, false)
        val pago = jobRepository.findPayments(id, startDate, endDate).map { it.salary }.sumOf { it }
        return PaymentDTO(pago, pago.compareTo(BigDecimal.ZERO) == 1)
    }

    open fun findAllInformationByJob(id: Long): List<EmployessInformation> {
        val a = fullInformationRepository.findAllByJob(id)

        return listOf()
    }

    open fun findAllEmployes(@Valid pageable: Pageable): List<Employess> {
        return empRepository.findAll().toList()
    }

    open fun findAllWorkers(@Valid pageable: Pageable): List<EmployessWorked> {
        return workerRepository.findAll().toList()
    }
}