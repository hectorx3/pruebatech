package example.micronaut.application

import example.micronaut.EmpleadoRepository
import example.micronaut.domain.Employess
import example.micronaut.infrastructure.EmployeeDTO
import jakarta.inject.Inject
import jakarta.inject.Singleton
import java.time.LocalDate
import java.time.Period

@Singleton
open class RegisterEmployeeUseCase
@Inject constructor(
    private val empRepository: EmpleadoRepository
) : UseCase<Employess, EmployeeDTO> {
    override fun handle(command: Employess): EmployeeDTO {
        var years = Period.between(command.birthdate, LocalDate.now()).years
        if(years > 17) {
            var findEmp = empRepository.findByNameAndLast_name(command.name, command.last_name)
            if(findEmp.isEmpty()) {
                val emp = empRepository.save(
                    command.gender_id,
                    command.job_id,
                    command.name,
                    command.last_name,
                    command.birthdate
                )
                return EmployeeDTO(emp.id?.toInt() ?: -1, true)
            }
        }
        return EmployeeDTO(-1, false)
    }
}