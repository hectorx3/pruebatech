package example.micronaut.infrastructure

import java.math.BigDecimal

data class EmployeeDTO(
    val id: Int,
    val success: Boolean
)

data class TotalHoursDTO(
    val total_worked_hours: Int,
    val success: Boolean
)

data class PaymentDTO(
    val payment: BigDecimal,
    val success: Boolean
)