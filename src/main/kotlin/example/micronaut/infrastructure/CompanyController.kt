package example.micronaut.infrastructure

import example.micronaut.application.QueryCompany
import example.micronaut.application.RegisterEmployeeUseCase
import example.micronaut.application.RegisterWorkedHoursUseCase
import example.micronaut.domain.*
import io.micronaut.data.model.Pageable
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.scheduling.TaskExecutors
import io.micronaut.scheduling.annotation.ExecuteOn
import jakarta.inject.Inject
import java.net.URI
import java.time.LocalDate
import javax.validation.Valid
import javax.validation.constraints.NotBlank

@ExecuteOn(TaskExecutors.IO)
@Controller("/company")
open class CompanyController(
    @Inject private val queryCompany: QueryCompany,
    @Inject private val registerEmployeUseCase: RegisterEmployeeUseCase,
    @Inject private val registerWorkedHoursUseCase: RegisterWorkedHoursUseCase
) {

    @Get("/gender/list") // <9>
    open fun list(@Valid pageable: Pageable): List<Genders> = //<10>
        queryCompany.findAllGenders(pageable)

    @Get("/job/{id}") // <9>
    open fun jobId(id: Long): List<EmployessInformation> = //<10>
        queryCompany.findAllInformationByJob(id)

    @Get("/job/list") // <9>
    open fun jobList(@Valid pageable: Pageable): List<Jobs> = //<10>
        queryCompany.findAllJobs(pageable)

    @Get("/employee/list") // <9>
    open fun emplist(@Valid pageable: Pageable): List<Employess> = //<10>
        queryCompany.findAllEmployes(pageable)

    @Post("/employee") // <11>
    open fun empSave(
        @Body("gender_id") @NotBlank gender_id: Long,
        @Body("job_id") @NotBlank job_id: Long,
        @Body("name") @NotBlank name: String,
        @Body("last_name") @NotBlank last_name: String,
        @Body("birthdate") @NotBlank birthdate: LocalDate,
    ) : HttpResponse<EmployeeDTO> {
        var emp = registerEmployeUseCase.handle(Employess(gender_id = gender_id, job_id = job_id, name = name, last_name = last_name, birthdate = birthdate))
        return HttpResponse
            .created(emp)
    }

    @Get("/worker/list")
    open fun worklist(@Valid pageable: Pageable): List<EmployessWorked> =
        queryCompany.findAllWorkers(pageable)

    @Get("/hours{?id,start,end}", produces = [MediaType.APPLICATION_JSON])
    open fun workerHours(id: Long?,
                         start: String?,
                         end: String?): TotalHoursDTO =
        queryCompany.getTotalHoursWorked(id, LocalDate.parse(start), LocalDate.parse(end))

    @Get("/payments{?id,start,end}")
    open fun paymentWorkerHours(id: Long?,
                                start: String?,
                                end: String?): PaymentDTO =
        queryCompany.getPaymentHoursWorked(id, LocalDate.parse(start), LocalDate.parse(end))


    @Post("/worker")
    open fun workSave(
        @Body("employee_id") @NotBlank employee_id: Long,
        @Body("worked_hours") @NotBlank worked_hours: Int,
        @Body("worked_date") @NotBlank worked_date: LocalDate
    ) : HttpResponse<EmployeeDTO> {
        val work = registerWorkedHoursUseCase.handle(EmployessWorked(employee_id = employee_id, worked_hours = worked_hours, worked_date = worked_date))

        return HttpResponse
            .created(work)
    }



    private val Long?.locationGender : URI
        get() = URI.create("/gender/$this")

    private val Genders.location : URI
        get() = id.locationGender

    private val Long?.locationJob : URI
        get() = URI.create("/job/$this")

    private val Jobs.locationJob : URI
        get() = id.locationGender
}