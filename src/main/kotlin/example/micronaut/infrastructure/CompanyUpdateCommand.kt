package example.micronaut.infrastructure

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.NotBlank

@Introspected // <1>
data class CompanyUpdateCommand(
    val id: Long,
    @field:NotBlank val name: String
)
