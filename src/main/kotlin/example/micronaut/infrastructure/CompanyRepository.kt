package example.micronaut

import example.micronaut.domain.*
import io.micronaut.context.annotation.Parameter
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.Query
import io.micronaut.data.exceptions.DataAccessException
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository
import io.micronaut.data.repository.GenericRepository
import io.micronaut.data.repository.PageableRepository
import java.math.BigDecimal
import java.time.LocalDate
import javax.transaction.Transactional
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@JdbcRepository(dialect = Dialect.POSTGRES) //<1>
abstract class GenderRepository : PageableRepository<Genders, Long> { //<2>

    abstract fun save(@NotBlank name: String) : Genders

    @Transactional
    open fun saveWithException(@NotBlank name: String): Genders {
        save(name)
        throw DataAccessException("test exception")
    }

    abstract fun update(@Id id: Long, @NotBlank name: String) : Long
}

@JdbcRepository(dialect = Dialect.POSTGRES)
abstract class FullInformationRepository : GenericRepository<Any, Long> {

    @Query("select * from employess e\n" +
            "join jobs j on j.id = e.job_id\n" +
            "join genders g on g.id = e.gender_id\n" +
            "where j.id = :jobid ", nativeQuery = true)
    abstract fun findAllByJob(@NotNull jobid: Long) : List<HashMap<Any, Any>>

}

    @JdbcRepository(dialect = Dialect.POSTGRES) //<1>
abstract class JobRepository : PageableRepository<Jobs, Long> { //<2>

    abstract fun save(@NotBlank name: String, @NotBlank salary: BigDecimal) : Jobs

    @Query("select j.id, j.\"name\", j.salary from employess_worked ew\n" +
            "join employess e on e.id = ew.employee_id\n" +
            "join jobs j on j.id = e.job_id \n" +
            "where employee_id = :empid \n" +
            "and worked_date >= :initDate \n" +
            "and worked_date <= :endDate ")
    abstract fun findPayments(@NotNull empid: Long,
                           @NotNull initDate: LocalDate,
                           @NotNull endDate: LocalDate
    ): List<Jobs>

    @Transactional
    open fun saveWithException(@NotBlank name: String, @NotBlank salary: BigDecimal): Jobs {
        save(name, salary)
        throw DataAccessException("test exception")
    }

    abstract fun update(@Id id: Long, @NotBlank name: String, @NotBlank salary: BigDecimal) : Long
}

@JdbcRepository(dialect = Dialect.POSTGRES) //<1>
abstract class EmpleadoRepository : PageableRepository<Employess, Long> { //<2>

    abstract fun findByNameAndLast_name(@NotBlank name: String, @NotBlank last_name: String) : List<Employess>

    abstract fun save(@NotBlank gender_id: Long,
                      @NotBlank job_id: Long,
                      @NotBlank name: String,
                      @NotBlank last_name: String,
                      @NotBlank birthdate: LocalDate
    ) : Employess

    @Transactional
    open fun saveWithException(@NotBlank gender_id: Long,
                               @NotBlank job_id: Long,
                               @NotBlank name: String,
                               @NotBlank last_name: String,
                               @NotBlank birthdate: LocalDate
    ): Employess {
        save(gender_id, job_id, name, last_name, birthdate)
        throw DataAccessException("test exception")
    }

    abstract fun update(@Id id: Long,
                        @NotBlank gender_id: Long,
                        @NotBlank job_id: Long,
                        @NotBlank name: String,
                        @NotBlank last_name: String,
                        @NotBlank birthdate: LocalDate
    ) : Long
}

@JdbcRepository(dialect = Dialect.POSTGRES) //<1>
abstract class WorkerRepository : PageableRepository<EmployessWorked, Long> { //<2>

    abstract fun findByEmployee_idAndWorked_date(@NotBlank employee_id: Long,
                                           @NotBlank worked_date: LocalDate) : List<EmployessWorked>


    @Query("select * from employess_worked ew where employee_id = :empid \n" +
            "and worked_date >= :initDate \n" +
            "and worked_date <= :endDate ")
    abstract fun findHoras(@NotNull empid: Long,
                           @NotNull initDate: LocalDate,
                           @NotNull endDate: LocalDate
    ): List<EmployessWorked>


    abstract fun save(@NotBlank employee_id: Long,
                      @NotBlank worked_hours: Int,
                      @NotBlank worked_date: LocalDate
    ) : EmployessWorked

    @Transactional
    open fun saveWithException(@NotBlank employee_id: Long,
                               @NotBlank worked_hours: Int,
                               @NotBlank worked_date: LocalDate
    ): EmployessWorked {
        save(employee_id, worked_hours, worked_date)
        throw DataAccessException("test exception")
    }

    abstract fun update(@Id id: Long,
                        @NotBlank worked_hours: Int
    ) : Long
}