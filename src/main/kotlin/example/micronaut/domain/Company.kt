package example.micronaut.domain

import io.micronaut.core.annotation.Introspected
import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import java.math.BigDecimal
import java.time.LocalDate

@MappedEntity
data class Genders(
    @field:Id
    @field:GeneratedValue(GeneratedValue.Type.AUTO)
    var id: Long? = null,
    var name: String
)

@MappedEntity
data class Jobs(
    @field:Id
    @field:GeneratedValue(GeneratedValue.Type.AUTO)
    var id: Long? = null,
    var name: String,
    var salary: BigDecimal
)

@MappedEntity
data class EmployessWorked(
    @field:Id
    @field:GeneratedValue(GeneratedValue.Type.AUTO)
    var id: Long? = null,
    var employee_id: Long,
    var worked_hours: Int,
    var worked_date: LocalDate
)

@MappedEntity
data class Employess(
    @field:Id
    @field:GeneratedValue(GeneratedValue.Type.AUTO)
    var id: Long? = null,
    var gender_id: Long,
    var job_id: Long,
    var name: String,
    var last_name: String,
    var birthdate: LocalDate
)

@Introspected
data class EmployessInformation(
    var id: Long? = null,
    var gender: Genders,
    var job: Jobs,
    var name: String,
    var last_name: String,
    var birthdate: LocalDate
)


